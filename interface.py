import os.path
from os import path
from database2 import *


def main():
    # Variable for while loop to continue until user decides to quit
    cont = 1

    # Checks to see if data is loaded
    # Need to write function here to check
    if path.exists("data.db") == True:
        dataLoaded = 1
    else:
        dataLoaded = 0

    # Checks if database is connected
    dataConnected = 0

    while cont == 1:
        # Get user input
        print("Type next command:")
        query = input()

        # Set everything to lowercase
        #TODO: Need to figure out how to make this work with the queries due to case sensitivity.
        #query = query.lower()

        # Break all words in the input up into individual words in a list for the logic
        statement = query.split(" ", 2)

        # Hardcoded commands
        if statement[0] == "help":
            print("-------------------------------------------------------------------------------------------------")
            print("| help                         shows this table                                                 |")
            print("| load data                    reads the csv files and creates the database                     |")
            print("| quit                         ends the program                                                 |")
            print("|                                                                                               |")
            print("| artist lookup “<artist>”     returns all data for the specified artist                        |")
            print("| artist songs “<artist>”      returns the number of songs artist <artist> produced in 2019     |")
            print("| artist age “<artist>”        returns the age of artist <artist>                               |")
            print("|                                                                                               |")
            print("| song lookup “<track_name>”   returns all data for <track_name>                                |")
            print("| song artist “<track_name>”   returns all artist data for the artist who produced <track_name> |")
            print("| song genre “<track_name>”    returns the genre of the song titled <track_name>                |")
            print("| song bpm “<track_name>”      returns the bpm of the song titled <track_name>                  |")
            print("|                                                                                               |")
            print("| all songs “<artist>”         returns all songs by <artist>                                    |")
            print("| all songs “<genre>”          returns all songs with genre <genre>                             |")
            print("| all songs “<bpm>”            returns all songs with BPM <bpm>                                 |")
            print("| all artists “<age>”          returns all artists <age> years old                              |")
            print("| all artists “<num_songs>”    returns all artists that produced <num_songs> in 2019            |")
            print("|                                                                                               |")
            print("| total artists                returns the total number of artists in the table                 |")
            print("| total songs                  returns the total number of songs in the table                   |")
            print("-------------------------------------------------------------------------------------------------")
        elif statement[0] == "quit":
            cont = 0
            print("Ending program")
        elif statement[0] == "load":
            if statement[1] == "data":
                print("Data has been loaded")
                dataLoaded = 1
                load_db()
            else:
                print("Unknown command. Type 'help' for help")
        # Stops user from doing any queries if the data is not loaded
        elif dataLoaded == 0:
            print("The data has not been loaded yet. Please run 'load data'")
        # Any point after this the elifs are queries
        elif statement[0] == "artist":
            if statement[1] == "lookup":
                artist_query(artist_name=statement[2])
                print("artist lookup")
            elif statement[1] == "songs":
                print("artist songs")
                # TODO: Need to add some stuff to database for this one.
            elif statement[1] == "age":
                print("artist age")
                artist_query(age= statement[2])
            else:
                print("Unknown command. Type 'help' for help")
        elif statement[0] == "song":
            if statement[1] == "lookup":
                song_query(track_name=statement[2])
            elif statement[1] == "artist":
                print("song artist")
                song_query(artist_name=statement[2])
            elif statement[1] == "genre":
                print("song genre")
                song_query(genre=statement[2])
            elif statement[1] == "bpm":
                song_query(bpm=statement[2])
            else:
                print("Unknown command. Type 'help' for help")
        elif statement[0] == "all":
            if statement[1] == "songs":
                try:
                    song_query(artist_name=statement[2])
                except BaseException:
                    print_songs_table()
                # do the logic here with the 2D array returned
            elif statement[1] == "artists":
                print("all artists")
                try:
                    if statement[2] == "age":
                        artist_query(age=statement[3])
                except BaseException:
                    print_artists_table()

            else:
                print("Unknown command. Type 'help' for help")
        elif statement[0] == "total":
            if statement[1] == "artists":
                print(artist_query(count=True, print_query=False))
            elif statement[1] == "songs":
                print("total songs")
        else:
            print("Unknown command. Type 'help' for help")


main()
