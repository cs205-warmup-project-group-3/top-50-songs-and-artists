## Project By Jonathan Knakal, Jonathan Thomas, Matt Gaetano, Ryan Martin
## For CS 205 Warmup Project Spring 2023
 
This program uses a command-line interface to query data that is stored in a relational database.
Specifically, the user can create and then query a database of songs and artists.
Upon running the program, the user must load the database by running 'load data'. This will only have to be done once, because the program will check if this data has been loaded in the past, and use that database.
The user then can use the query language as shown below to get data from the database. These can also be accessed by typing 'help' when the program is being run

    help                       shows this table
    quit                       ends the program
    
    artist lookup <artist>     returns all data for the specified artist
    artist songs <artist>      returns the number of songs artist <artist> produced in 2019
    artist age <artist>        returns the age of artist <artist>
    
    song lookup <track_name>   returns all data for <track_name>
    song artist <track_name>   returns all artist data for the artist who produced <track_name>
    song genre <track_name>    returns the genre of the song titled <track_name>
    song bpm <track_name>      returns the bpm of the song titled <track_name>
    
    all songs <artist>         returns all songs by <artist>
    all songs <genre>          returns all songs with genre <genre>
    all songs <bpm>            returns all songs with BPM <bpm>
    all artists <age>          returns all artists <age> years old
    all artists <num_songs>    returns all artists that produced <num_songs> in 2019
    
    total artists              returns the total number of artists in the table
    total songs                returns the total number of songs in the table

The main program to run is interface.py. This then uses database2.py to create the database data.db using artists.csv and songs.csv
Once the database is created using 'load data', this command does not have to be run again. This program will establish a connection to the database, and continue to use that connection for any future queries. 
