def make_sql_table(conn, df, pk='ID', fk=None, fk_ref=None, fk_ref_col=None, tbl_name='tbl_test'):
    '''
    Creates SQL code for making a table like a pandas dataframe where
    you want a specific field to be the primary key and or the foreign key.
    :param conn: connection to SQLite database
    :param df: dataframe we want to make a table like
    :param pk: string name of the field to be used as a primary key
    :param fk: optional string name of the field to be used as a foreign key
    :param fk_ref: string name of the table the fk references
    :param tbl_name: string name for the table to be created'''

    dtypes = {"<class 'numpy.float64'>": 'REAL', "<class 'numpy.float32'>": 'REAL',
              "<class 'numpy.float16'>": 'REAL', "<class 'numpy.uint64'>": 'INTEGER',
              "<class 'numpy.uint32'>": 'INTEGER', "<class 'numpy.uint16'>": 'INTEGER',
              "<class 'numpy.uint8'>": 'INTEGER', "<class 'numpy.int64'>": 'INTEGER',
              "<class 'numpy.int32'>": 'INTEGER', "<class 'numpy.int16'>": 'INTEGER',
              "<class 'numpy.int8'>": 'INTEGER', "<class 'str'>": 'TEXT',
              "<class 'int'>": 'INTEGER', "<class 'float'>": 'REAL'}

    cols = list(df.columns)
    #print(cols)

    if pk != None:
        cols.remove(pk)
        pkcol = pk
        pkdtype = dtypes[str(type(df[pkcol].iloc[0]))]
    # else:
    #         df['pk'] = [x for x in range(df.shape[0])]
    #         pk = 'pk'

    table_name = tbl_name

    fkcol = fk

    if fk != None:
        cols.remove(fk)
        fkdtype = dtypes[str(type(df[fkcol].iloc[0]))]

    col_list = [col for col in cols]
    dtype_list = [str(type(df[col].iloc[0])) for col in cols]

    col_list = ", ".join([str(col_list[col]) + ' ' + dtypes[str(dtype_list[col])] for col in range(len(col_list))])
    create_table = ('CREATE TABLE %s (' % tbl_name)
    if pk != None:
        primary_key = ('%s %s PRIMARY KEY NOT NULL, ' % (pkcol, pkdtype))
    else:
        primary_key = ''

    if fk != None:
        foreign_key = (
        ',%s %s NOT NULL, FOREIGN KEY (%s) REFERENCES %s(%s) ' % (fkcol, fkdtype, fk, fk_ref, fk_ref_col))

    if fk == None:
        sql = create_table + primary_key + col_list + ');'
    else:
        sql = create_table + primary_key + col_list + foreign_key + ');'

    cur = conn.cursor()
    cur.executescript(sql)
    df.to_sql(tbl_name, conn, if_exists='append', index=False)
    print(sql)