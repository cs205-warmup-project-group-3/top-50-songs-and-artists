
import sqlite3
import pandas as pd
import string
from tabulate import tabulate


ARTIST_COLUMNS = ['ID', 'Artist Name', '# of Songs Produced', 'Age']
SONG_COLUMNS = ['Top 1 - 50', 'Track Name', 'Artist Name', 'Genre', 'BPM']

#TODO: Ask Jason how to use one SQL connection for all queries.

def load_db():
    """
    Loads artists.csv & songs.csv into two tables in data.db usings the pandas libary

    """
    try:
        songs_df = pd.read_csv('songs.csv', encoding='latin1')
        artists_df = pd.read_csv('artists.csv', encoding='latin1')
        con = sqlite3.connect("data.db")

        # make_sql_table(con, songs_df, 'id', 'artist_name', 'artists', 'songs', 'songs')
        # make_sql_table(con, artists_df, 'ï»¿artist_id', 'artist_name', 'songs', 'artist_name', 'artists')
        songs_df.to_sql('songs', con, if_exists='fail', index=False)
        artists_df.to_sql('artists', con, if_exists='fail', index=False)
        con.commit()
        con.close()
        return True
    except sqlite3.OperationalError:
        print("Database already exists.")
    except UnicodeDecodeError:
        print("CSV Encoding Error.")
    except ValueError:
        print("Database already exsists. ")


def print_songs_table():
    """
    Prints the whole song table from data.db, formatted using the tabulate libary.

    """
    # Create SQL connection.
    con = sqlite3.connect("data.db")
    c = con.cursor()

    # Put column names in a list. The column needs are currently hardcoded so this is unneeded.
    c.execute("SELECT * FROM songs")
    result = c.fetchall()

    # Put column names in a list. The column needs are currently hardcoded so this is unneeded.
    # c.execute("SELECT * FROM songs limit 1")
    # col_name = [i[0] for i in c.description]

    # Uses tabulate libary to print our tables nicely.
    print(tabulate(result, headers=SONG_COLUMNS, tablefmt='psql'))


def print_artists_table():
    """
    Prints the whole artists table from data.db, formatted using the tabulate libary.

    """
    try:
        # Create SQL connnection.
        con = sqlite3.connect("data.db")
        c = con.cursor()

        # Grab data from the table.
        c.execute("SELECT * FROM artists")
        result = c.fetchall()

        # Put column names in a list. The column needs are currently hardcoded so this is unneeded.
        # c.execute("SELECT * FROM artists limit 1")
        # col_name = [i[0] for i in c.description]

        # Uses tabulate libary to print our tables nicely.
        print(tabulate(result, headers=ARTIST_COLUMNS, tablefmt='psql'))
    except BaseException:
        print("Artists table failed to print.")


def artist_query(artist_id=None, artist_name=None, num_songs_produced=None, age=None, print_query=True, count=False):
    """
    Querys the artists table from data.db

    :param artist_id: INT
    :param artist_name: STRING
    :param num_songs_produced: INT
    :param age: INT
    :param print_query: BOOL True if you want the query results to print, false otherwise. The function will still return an array.
    :param count: set to True if you want the query to return an array of the total count.

    Returns a array of queries information.

    note: if count is True then the query will not return any other information.

     """
    try:
        # Create database connection.
        conn = sqlite3.connect('data.db')
        c = conn.cursor()

        # Returns the count of count = True
        if count:
            c.execute("SELECT COUNT(artist_name) FROM artists")
            count = c.fetchall()
            print("Total Artists: " + str(count[0][0]))
        # Remove parenthesis for SQL query.
        if (artist_name != None):
            artist_name = artist_name.translate(str.maketrans('', '', string.punctuation))
        # Queries table using function variables.
        c.execute(
            "SELECT * FROM artists WHERE ï»¿id = ?  OR artist_name = ? OR num_songs_produced = ? OR age = ?",
            [(artist_id), (artist_name), (num_songs_produced), (age)])
        result = c.fetchall()

        # Prints a formatted query if print table = True
        if print_query:
            print(tabulate(result, headers=ARTIST_COLUMNS))
        c.close()

        # Returns an array.
        return result
    except BaseException:
        print("Artist table query unsuccessful.")


def song_query(id=None, track_name=None, artist_name=None, genre=None, bpm=None, print_query=True, count=False):
    """
    Querys the song table from data.db

    :param id: INT
    :param artist_name: STRING
    :param genre: STRING
    :param bpm: INT
    :param print_query: BOOL, True if you want the query results to print, false otherwise. The function will still return an array.
    :param count: BOOL, set to True if you want the query to return an array of the total artists.

    Returns a array of queries information.

    note: if count is True then the query will not return any other information.

     """
    try:
        # Create database connection.
        conn = sqlite3.connect('data.db')
        c = conn.cursor()
        # Returns the count of count = True
        if count:
            c.execute("SELECT COUNT(id) FROM songs")
            count = c.fetchall()
            print("Total Songs: " + str(count[0][0]))

        # Remove parenthesis for SQL query.
        if (genre != None):
            genre = genre.translate(str.maketrans('', '', string.punctuation))
        if (track_name != None):
            track_name = track_name.translate(str.maketrans('', '', string.punctuation))
        if (artist_name != None):
            artist_name = artist_name.translate(str.maketrans('', '', string.punctuation))
        # Queries table using function variables.
        c.execute("SELECT * FROM songs WHERE id = ? OR track_name = ? OR genre = ? OR bpm = ? OR artist_name = ?",
                  [(id), (track_name), (genre), (bpm), (artist_name)])
        result = c.fetchall()

        # Prints a formatted query if print_query = True
        if print_query:
            print(tabulate(result, headers= SONG_COLUMNS))
        c.close()
        return result
    except BaseException:
        print("Song table query unsuccessful.")
