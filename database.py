"""
Modified from code by Jim Eddy

"""
import sqlite3

def create_songs_table():
    """ Create table 'songs' in 'warmup_database' database """
    try:
        conn = sqlite3.connect('warmup_database.db')
        c = conn.cursor()
        c.execute('''CREATE TABLE songs
                    (
                    id integer,
                    track_name text,
                    artist_name text,
                    genre text,
                    bpm integer
                    )''')
        conn.commit()
        return True
    except BaseException:
        return False
    finally:
        if c is not None:
            c.close()
        if conn is not None:
            conn.close()

def create_artists_table():
    """ Create table 'artists' in 'warmup_database' database """
    try:
        conn = sqlite3.connect('warmup_database.db')
        c = conn.cursor()
        c.execute('''CREATE TABLE artists
                    (
                    id integer,
                    artist_name text,
                    num_songs integer,
                    age integer
                    )''')
        conn.commit()
        return True
    except BaseException:
        return False
    finally:
        if c is not None:
            c.close()
        if conn is not None:
            conn.close()

def add_song(id, name, artist, genre, bpm):
    """ Example data insert into accounts table """
    data_to_insert = [(id, name, artist, genre, bpm)]
    try:
        conn = sqlite3.connect('warmup_database.db')
        c = conn.cursor()
        c.executemany("INSERT INTO songs VALUES (?, ?, ?, ?, ?)", data_to_insert)
        conn.commit()
    except sqlite3.IntegrityError:
        print("Error. Tried to add duplicate record!")
    else:
        print("Success")
    finally:
        if c is not None:
            c.close()
        if conn is not None:
            conn.close()

def add_artist(id, name, songs, age):
    """ Example data insert into accounts table """
    data_to_insert = [(id, name, songs, age)]
    try:
        conn = sqlite3.connect('warmup_database.db')
        c = conn.cursor()
        c.executemany("INSERT INTO artists VALUES (?, ?, ?, ?)", data_to_insert)
        conn.commit()
    except sqlite3.IntegrityError:
        print("Error. Tried to add duplicate record!")
    else:
        print("Success")
    finally:
        if c is not None:
            c.close()
        if conn is not None:
            conn.close()

def get_song(track_name):
    """ Return array of song data """
    return_array = []
    try:
        conn = sqlite3.connect('warmup_database.db')
        c = conn.cursor()
        for row in c.execute("SELECT * FROM songs"):
            if row[1] == track_name:
                return_array.append(row)
    except sqlite3.DatabaseError:
        print("Error. Could not retrieve data.")
    finally:
        if c is not None:
            c.close()
        if conn is not None:
            conn.close()

    return return_array

def get_songs(artist_name):
    """ Return array of song data """
    return_array = []
    try:
        conn = sqlite3.connect('warmup_database.db')
        c = conn.cursor()
        for row in c.execute("SELECT * FROM songs"):
            if row[2] == artist_name:
                return_array.append(row)
    except sqlite3.DatabaseError:
        print("Error. Could not retrieve data.")
    finally:
        if c is not None:
            c.close()
        if conn is not None:
            conn.close()

    return return_array

def get_artist(artist_name):
    """ Return array of artist data """
    return_array = []
    try:
        conn = sqlite3.connect('warmup_database.db')
        c = conn.cursor()
        for row in c.execute("SELECT * FROM artists"):
            if row[1] == artist_name:
                return_array.append(row)
    except sqlite3.DatabaseError as e:
        print("Error. Could not retrieve data.")
        print(e)
    finally:
        if c is not None:
            c.close()
        if conn is not None:
            conn.close()

    return return_array

# create_songs_table()
# create_artists_table()
# add_song(0, "Senorita", "Shawn Mendes", "canadian pop", 117)
# add_artist(0, "Shawn Mendes", 5, 24)
# add_song(1, "Old Town Road", "Lil Nas X", 0, 0)
# add_song(2, "Stitches", "Shawn Mendes", 0, 0)
print(get_songs("Shawn Mendes"))
print(get_artist("Shawn Mendes"))